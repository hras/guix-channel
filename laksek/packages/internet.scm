(define-module (laksek packages internet)
  #:use-module (guix packages)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz))

(define-public python-basc-py4chan
  (package
    (name "python-basc-py4chan")
    (version "0.6.6")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "BASC-py4chan" version))
       (sha256
        (base32 "0v1vb7c0x1kfkz1cl0wxky2bm8jjkr8nivx56rqcayfvjgwnk5vi"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f))
    (propagated-inputs `(("python-requests" ,python-requests)))
    (home-page "https://github.com/bibanon/BASC-py4chan")
    (synopsis "Python 4chan API Wrapper")
    (description "Python 4chan API Wrapper.  Improved version
of Edgeworth's original py-4chan wrapper.")
    (license license:wtfpl2)))

(define-public python-basc-archiver
  (package
    (name "python-basc-archiver")
    (version "0.9.9")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "BASC-Archiver" version))
       (sha256
        (base32 "1rm91zj4asx3987g35bxsp7kcy3b0nlpnnr4j3yr53g5sl0n17d0"))))
    (build-system python-build-system)
    (propagated-inputs `(("python-basc-py4chan" ,python-basc-py4chan)
                         ("python-docopt" ,python-docopt)
                         ("python-requests" ,python-requests)))
    (home-page "https://github.com/bibanon/BASC-Archiver")
    (synopsis "4chan imageboard thread archiver")
    (description
     "Makes a complete archive of imageboard threads including images, HTML, and JSON.")
    (license license:lgpl3+)))
