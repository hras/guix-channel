(define-module (laksek packages texlive)
  #:use-module ((srfi srfi-1)
                #:hide (zip))
  #:use-module (gnu packages tex)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system texlive)
  #:use-module ((guix licenses)
                #:prefix license:))

;; (define* (simple-texlive-package name locations hash
;;                                  #:key trivial?)
;;   "Return a template for a simple TeX Live package with the given NAME,
;; downloading from a list of LOCATIONS in the TeX Live repository, and expecting
;; the provided output HASH.  If TRIVIAL? is provided, all files will simply be
;; copied to their outputs; otherwise the TEXLIVE-BUILD-SYSTEM is used."
;;   (define with-documentation?
;;     (and trivial?
;;          (any (lambda (location)
;;                 (string-prefix? "/doc" location)) locations)))
;;   (package
;;     (name name)
;;     (version (number->string %texlive-revision))
;;     (source
;;      (texlive-origin name version locations hash))
;;     (outputs (if with-documentation?
;;                  '("out" "doc")
;;                  '("out")))
;;     (build-system (if trivial? gnu-build-system texlive-build-system))
;;     (arguments
;;      (let ((copy-files `(lambda* (#:key outputs inputs #:allow-other-keys)
;;                           (let (,@(if with-documentation?
;;                                       `((doc (string-append (assoc-ref outputs
;;                                                              "doc")
;;                                               "/share/texmf-dist/")))
;;                                       '()) (out (string-append (assoc-ref
;;                                                                 outputs "out")
;;                                                  "/share/texmf-dist/")))
;;                             ,@(if with-documentation?
;;                                   '((mkdir-p doc)
;;                                     (copy-recursively (string-append (assoc-ref
;;                                                                       inputs
;;                                                                       "source")
;;                                                                      "/doc")
;;                                                       (string-append doc
;;                                                                      "/doc")))
;;                                   '())
;;                             (mkdir-p out)
;;                             (copy-recursively "." out)
;;                             ,@(if with-documentation?
;;                                   '((delete-file-recursively (string-append
;;                                                               out "/doc")))
;;                                   '()) #t))))
;;        (if trivial?
;;            `(#:tests? #f
;;              #:phases (modify-phases %standard-phases
;;                         (delete 'configure)
;;                         (replace 'build
;;                           (const #t))
;;                         (replace 'install
;;                           ,copy-files)))
;;            `(#:phases (modify-phases %standard-phases
;;                         (add-after 'install 'copy-files
;;                           ,copy-files))))))
;;     (home-page #f)
;;     (synopsis #f)
;;     (description #f)
;;     (license #f)))

;; (define texlive-chemgreek
;;   (package
;;     (inherit (simple-texlive-package "texlive-chemgreek"
;;                                      (list "doc/latex/chemgreek/"
;;                                            "tex/latex/chemgreek/")
;;                                      (base32
;;                                       "0rpcywz5w2im5ck0kp56dywiy1his01s817pngdf9xdn02qk517n")
;;                                      #:trivial? #t))
;;     (version "1.1a")
;;     (home-page "https://ctan.org/macros/latex/contrib/chemgreek")
;;     (synopsis "Upright Greek letters in chemistry")
;;     (description
;;      "The package provides upright Greek letters in support of other chemistry
;; packages (such as chemmacros).  The package used to be distributed as a part of
;; chemmacros.")
;;     (license license:lppl1.3+)))

;; (define-public texlive-mhchem
;;   (package
;;     (inherit (simple-texlive-package "texlive-mhchem"
;;                                      (list "doc/latex/mhchem/"
;;                                            "tex/latex/mhchem/")
;;                                      (base32
;;                                       "1w60p2nd7p3hgg3hjvlzc6hakfnhk3vbbpb1r95yk5xznhq5r09l")
;;                                      #:trivial? #t))
;;     (version "2021.12.31")
;;     (propagated-inputs (list texlive-tools
;;                              texlive-l3packages
;;                              texlive-l3kernel
;;                              texlive-graphics-def
;;                              texlive-chemgreek
;;                              texlive-amsmath))
;;     (home-page "https://ctan.org/macros/latex/contrib/mhchem")
;;     (synopsis
;;      "Typeset chemical formulae/equations and Risk and Safety phrases")
;;     (description
;;      "The bundle provides three packages: The mhchem package provides commands fortypesetting chemical molecular formulae and equations.  The hpstatement package
;; provides commands for the official hazard statements and precautionary
;; statements (H and P statements) that are used to label chemicals.  The rsphrase
;; package provides commands for the official Risk and Safety (R and S) Phrases
;; that are used to label chemicals.  The package requires the expl3 bundle.")
;;     (license license:lppl1.3c)))

;; (define texlive-simplekv
;;   (package
;;     (inherit (simple-texlive-package "texlive-simplekv"
;;                                      (list "doc/generic/simplekv/"
;;                                            "tex/generic/simplekv/")
;;                                      (base32
;;                                       "0q0bgrrj4kdhzkhlkfraj9niznch0jnfdyamznm6pskkwc1rlkjs")
;;                                      #:trivial? #t))
;;     (version "0.2a")
;;     (home-page "https://ctan.org/macros/generic/simplekv")
;;     (synopsis "A simple key/value system for TeX and LaTeX")
;;     (description
;;      "The package provides a simple key/value system for TeX and LaTeX.")
;;     (license license:lppl1.3c)))

;; (define-public texlive-chemfig
;;   (package
;;     (inherit (simple-texlive-package "texlive-chemfig"
;;                                      (list "doc/generic/chemfig/"
;;                                            "tex/generic/chemfig/")
;;                                      (base32
;;                                       "0vglspm69hl5vg1j842wd9021h96h9crliplabikrnbaarq5dy5w")
;;                                      #:trivial? #t))
;;     (version "1.6c")
;;     (propagated-inputs (list texlive-simplekv))
;;     (home-page "https://ctan.org/macros/generic/chemfig")
;;     (synopsis "Draw molecules with easy syntax")
;;     (description
;;      "The package provides the command \\chemfig{<code>}, which draws molecules using
;; the TikZ package.  The <code> argument provides instructions for the drawing
;; operation.  While the diagrams produced are essentially 2-dimensional, the
;; package supports many of the conventional notations for illustrating the
;; 3-dimensional layout of a molecule.  The package uses TikZ for its actual
;; drawing operations.")
;;     (license license:lppl1.3c)))
