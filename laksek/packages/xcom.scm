(define-module (laksek packages xcom)
  #:use-module ((guix licenses)
		#:prefix license:)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sdl)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake))

(define-public openxcom-extended
  (package
   (name "openxcom-extended")
   (version "7.13.10")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/MeridianOXC/OpenXcom")
           (commit "27269a0ef3ce6253efac9297cb973a54a4127608")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0vd9l0yp41bpvqg2ihn7zx5d7lm1766iaixlcvqrm3alajf58mv8"))))
   (build-system cmake-build-system)
   (arguments
     (list
      #:tests? #f
      #:build-type "Release"
      #:configure-flags #~'("-DDEV_BUILD=OFF"
			    "-DBUILD_PACKAGE=OFF")))
   (inputs (list glu
		 (sdl-union (list sdl sdl-gfx sdl-image sdl-mixer))
		 yaml-cpp))
   (native-inputs (list pkg-config))
   (synopsis "Open-source clone of the original X-Com")
   (description "OpenXcom is an open-source clone of the popular UFO: Enemy Unknown  and X-COM: Terror From the Deep videogames by Microprose, licensed under the GPL and written in C++ / SDL.")
   (home-page "https://openxcom.org")
   (properties '((tunable? . #t)))
   (license license:gpl3+)))
