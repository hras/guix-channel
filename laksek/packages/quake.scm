(define-module (laksek packages quake)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages games)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph))

(define-public ironwail
  (package
    (name "ironwail")
    (version "0.7.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/andrei-drexler/ironwail")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1xsfn3jp2imgqabci7hh2y7aw4yiv4w3lgkddaiqbkryqmlx18pg"))))
    (arguments
     `(#:tests? #f
       #:make-flags (list "MP3LIB=mpg123"
                          "USE_CODEC_FLAC=1"
                          "USE_CODEC_MIKMOD=1"
                          "USE_CODEC_OPUS=1"
                          "-CQuake"
                          (string-append "CC="
                                         ,(cc-for-target)))
       #:phases (modify-phases %standard-phases
                  (delete 'configure)
                  (add-after 'unpack 'fix-makefile-paths
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out")))
                        (mkdir-p (string-append out "/bin"))
                        (substitute* "Quake/Makefile"
                          (("/usr/local/games/quake")
                           (string-append out "/bin"))) #t))))))
    (build-system gnu-build-system)
    (native-inputs (list pkg-config))
    (inputs (list curl
                  alsa-lib
                  libmikmod
                  libvorbis
                  opusfile
                  flac
                  mesa
                  mpg123
                  sdl2))
    (home-page "https://github.com/andrei-drexler/ironwail")
    (synopsis "High-performance QuakeSpasm Fork")
    (description
     "A fork of the popular GLQuake descendant QuakeSpasm with a focus
on high performance instead of maximum compatibility")
    (license license:gpl2)))
