(define-module (laksek packages roguelike)
  #:use-module (guix packages)
  #:use-module (guix git-download)  
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages sdl)
  #:use-module (guix build-system cmake))

(define-public infra-arcana
  (package
    (name "infra-arcana")
    (version "22.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://gitlab.com/martin-tornqvist/ia.git")
	     (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32 "01x1pjllzf96nkhfqz4kfx3yn6nxxhq8gxklc54qszxm9wgv13rh"))))
    (build-system cmake-build-system)
    (inputs (list sdl2
                  sdl2-image
                  sdl2-mixer))
    (synopsis "A Roguelike set in the early 20th century")
    (description "Infra Arcana is a Roguelike set in the early 20th century. The goal is to explore the lair of a dreaded cult called The Church of Starry Wisdom. Buried deep beneath their hallowed grounds lies an artefact called The Shining Trapezohedron - a window to all secrets of the universe. Your ultimate goal is to unearth this artefact.")
    (home-page "https://sites.google.com/site/infraarcana/")
    (license license:agpl3+)))
