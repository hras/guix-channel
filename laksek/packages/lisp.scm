(define-module (laksek packages lisp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages c)
  #:use-module (gnu packages perl)
  #:use-module ((guix licenses)
                #:prefix license:))

(define cyclone-bootstrap
  (package
    (name "cyclone-bootstrap")
    (version "v0.35.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/justinethier/cyclone-bootstrap")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0k7j2sjfazsmcw3s7p13w1yyibvlr8dsvjh25spbiadk466layfq"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (delete 'configure))
       #:make-flags (list (string-append "PREFIX="
                                         (assoc-ref %outputs "out"))
                          (string-append "CC="
                                         ,(cc-for-target)))
       #:test-target "test"))
    (native-inputs (list perl))
    (inputs (list ck))
    (home-page "http://justinethier.github.io/cyclone/")
    (synopsis "Cyclone Scheme bootstrap compiler")
    (description
     "Cyclone Scheme is a brand-new compiler that allows
      real-world application development using the R7RS Scheme Language standard.
      The Cyclone compiler is self-hosted and as such needs to be
      boostraped using this compiler")
    (license license:expat)))

(define-public cyclone
  (package
    (name "cyclone")
    (version "v0.35.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/justinethier/cyclone")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1fv0k9l8qcjba7qyig6w1jv72ngcav66031k5aciby63nhlycmzc"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (delete 'configure))
       #:make-flags (list (string-append "PREFIX="
                                         (assoc-ref %outputs "out"))
                          (string-append "CC="
                                         ,(cc-for-target)))
       #:test-target "test"))
    (native-inputs (list perl))
    (inputs (list ck cyclone-bootstrap))
    (home-page "http://justinethier.github.io/cyclone/")
    (synopsis "The Cyclone Scheme Compiler")
    (description
     "Cyclone Scheme is a brand-new compiler that allows real-world application
      development using the R7RS Scheme Language standard.  We provide modern
      features and a stable system capable of generating fast native binaries.
      Cheney on the MTA is used by Cyclone’s runtime to implement full tail
      recursion, continuations, and generational garbage collection.  In addition,
      the Cheney on the MTA concept has been extended to allow execution of
      multiple native threads.  An on-the-fly garbage collector is used to manage
      the second-generation heap and perform major collections without
      \"stopping the world\".")
    (license license:expat)))
