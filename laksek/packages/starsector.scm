(define-module (laksek packages starsector)
  #:use-module (guix packages)
  #:use-module (nonguix download)
  #:use-module (games packages starsector))

(define-public starsector-dl-fix
  (package
    (inherit starsector)
    (name "starsector-fix")
    (version "0.97a-RC11")
    (source
     (origin
       (method unredistributable-url-fetch)
       (uri (string-append
             "https://f005.backblazeb2.com/file/fractalsoftworks/release/starsector_linux-"
             version ".zip"))
       (sha256
        (base32 "15md074szd4zabzrl00v5rb58k0cwxp1fw1wcxccbg6rq8k85mgf"))))))
