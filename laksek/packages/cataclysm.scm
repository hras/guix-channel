(define-module (laksek packages cataclysm)
  #:use-module (gnu packages code)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages xiph)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public cataclysm-bn
  (package
    (name "cataclysm-bn")
    (version "v0.5.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/cataclysmbnteam/Cataclysm-BN.git")
             (commit "v0.5.2")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "015inc781mm189bzzpiakinm5bl64cf401fsh6djp35nzy99simv"))))
    (build-system gnu-build-system)
    (arguments
     '(#:make-flags (list (string-append "PREFIX="
                                         (assoc-ref %outputs "out"))
                          "RELEASE=1"
                          "TILES=1"
                          "SOUND=1"
                          "USE_XDG_DIR=1"
                          "DYNAMIC_LINKING=1"
			  "LUA=1"
                          "VERSION=v0.5.2")
       #:phases (modify-phases %standard-phases
                  (delete 'configure))
       #:tests? #f))
    (native-inputs (list gettext-minimal pkg-config astyle))
    (inputs (list freetype
                  git-minimal
                  gnuplot
                  libogg
                  libvorbis
                  ncurses
                  perl
                  python
                  python-2
                  r
                  sdl2
                  sdl2-image
                  sdl2-ttf
                  sdl2-mixer))
    (home-page "https://github.com/cataclysmbnteam/Cataclysm-BN")
    (synopsis "Survival horror roguelike video game")
    (description
     "Cataclysm: Bright Nights is a fork of Cataclysm: Dark Days Ahead
       (or \"DDA\" for short), which is a roguelike set in a post-apocalyptic world.
       Struggle to survive in a harsh, persistent, procedurally generated world.
       Scavenge the remnants of a dead civilization for food, equipment,
       or, if you are lucky, a vehicle with a full tank of gas to get you out of Dodge.
       Fight to defeat or escape from a wide variety of powerful monstrosities,
       from zombies to giant insects to killer robots and things
       far stranger and deadlier, and against the others like yourself,
       that want what you have.")
    (license license:cc-by-sa3.0)))

(define-public cataclysm-bn-experimental
  (package
   (inherit cataclysm-bn)
   (name "cataclysm-bn")
   (version "2024-02-27")
   (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/cataclysmbnteam/Cataclysm-BN.git")
             (commit "2024-02-27")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1mk1jari6q38f32w4a0ir9727rindfdbg8qn98ykfyz805h1an7z"))))
   (arguments
     '(#:make-flags (list (string-append "PREFIX="
                                         (assoc-ref %outputs "out"))
                          "RELEASE=1"
                          "TILES=1"
                          "SOUND=1"
                          "USE_XDG_DIR=1"
                          "DYNAMIC_LINKING=1"
			  "LUA=1"
                          "VERSION=2024-02-27")
       #:phases (modify-phases %standard-phases
                  (delete 'configure))
       #:tests? #f))))
