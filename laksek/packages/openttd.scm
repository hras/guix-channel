(define-module (laksek packages openttd)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)  
  #:use-module (gnu packages games)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages fcitx)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sdl)
  #:use-module (guix build-system cmake))

(define-public openttd-jgrpp  
  (package
   (inherit openttd)
   (name "openttd-jgrpp")
   (version "0.58.3")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/JGRennison/OpenTTD-patches")
	   (commit (string-append "jgrpp-" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32 "1pxvdvv02agkqmsi33i8rrfp2fg3y2cqxr3zikaqfyljp1krs41m"))))
   (arguments
    (list
     #:tests? #f
     #:configure-flags
     #~(list (string-append "-DCMAKE_INSTALL_BINDIR=" #$output "/bin"))
     #:phases
     #~(modify-phases %standard-phases
		      (add-after 'unpack 'patch-sources
				 (lambda* (#:key inputs #:allow-other-keys)
				   (substitute* (list "src/music/fluidsynth.cpp"
						      "CMakeLists.txt")
						(("default_sf\\[\\] = \\{" all)
						 (string-append all "\""(search-input-file inputs "/share/soundfonts/FreePatsGM.sf2") "\","))
						(("HARFBUZZ" all)
						 "Harfbuzz"))))
		      (add-before 'check 'install-data
				  (lambda* (#:key inputs outputs #:allow-other-keys)
				    (let ((base "/share/games/openttd"))
				      (for-each
				       (lambda (dir)
					 (copy-recursively
					  (string-drop-right dir (string-length base))
					  #$output))
				       (search-path-as-list (list base) (map cdr inputs)))))))))
   (inputs
    (modify-inputs (package-inputs openttd)
		   (prepend curl
			    dbus
			    doxygen
			    fcitx
			    graphviz
			    harfbuzz
			    pkg-config
			    `(,zstd "lib"))
		   (replace "sdl" sdl2)
		   (replace "allegro" allegro-4)))
   (synopsis "JGR's Patchpack for OpenTTD")))
