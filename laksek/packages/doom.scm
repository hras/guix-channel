(define-module (laksek packages doom)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages music)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xiph)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu))

(define-public chocolate-doom
  (package
    (name "chocolate-doom")
    (version "3.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/chocolate-doom/chocolate-doom")
	     (commit (string-append "chocolate-doom-" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32 "11dam4nz8p4wndflkyykr2cfcrm30qq21b0877hv0idk5fldycy8"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:configure-flags #~'("CFLAGS=-fcommon")
      #:phases
      #~(modify-phases %standard-phases
          (replace 'bootstrap
            ;; The bundled autogen.sh script unconditionally runs ./configure.
            (lambda _ (invoke "autoreconf" "-vif"))))))
    (inputs (list libsamplerate
		  libpng
		  (sdl-union (list sdl2 sdl2-mixer sdl2-net))))
    (native-inputs (list automake
			 autoconf
			 pkg-config
			 python))
    (synopsis "Source port of DOOM")
    (description "Chocolate Doom is a Doom source port that accurately reproduces the experience of Doom as it was played in the 1990s.")
    (home-page "https://www.chocolate-doom.org/")
    (properties '((tunable? . #t)))
    (license license:gpl2+)))

(define-public crispy-doom
  (package
    (inherit chocolate-doom)
    (name "crispy-doom")
    (version "7.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/fabiangreffrath/crispy-doom")
	     (commit (string-append "crispy-doom-" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32 "1g1jkkvrvg0fasl5wgsffffzwqx6h68wafc5f36kq0h6psqmkczs"))))
    (description "Crispy Doom is a limit-removing enhanced-resolution Doom source port based on Chocolate Doom.")
    (home-page "https://github.com/fabiangreffrath/crispy-doom")))

(define-public dsda-doom
  (package
    (name "dsda-doom")
    (version "0.28.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/kraflab/dsda-doom")
	     (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32 "04z2m2f3dy630rlwrsjs2mwrr2av52sqbdbhiccpwqc8m1wgsssz"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:tests? #f
      #:build-type "Release"
      #:phases #~(modify-phases %standard-phases
		   (add-before 'configure 'chdir-src
		     (lambda _ (chdir "prboom2"))))))
    (inputs (list dumb
		  fluidsynth
		  glu
		  libmad
		  libzip
		  portmidi
		  (sdl-union (list sdl2 sdl2-image sdl2-mixer))
		  vorbis-tools))
    (native-inputs (list pkg-config))
    (synopsis "Source port of DOOM")
    (description "dsda-doom is a successor of prboom+ with many new features, including MBF21, UDMF, and MAPINFO support.")
    (home-page "https://github.com/kraflab/dsda-doom")
    (properties '((tunable? . #t)))
    (license license:gpl2+)))

(define-public nyan-doom
  (package
    (inherit dsda-doom)
    (name "nyan-doom")
    (version "1.1.9")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/andrikpowell/nyan-doom")
	     (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32 "0zp811p209700pxdmjj841xmraxp9d53rj8b71ifjvwkipj3q33c"))))
    (description "This is Arsinikk's own personal fork of DSDA-Doom with new added features and improvements. It's kept relatively up to date with the original.")
    (home-page "https://github.com/andrikpowell/nyan-doom")))

(define-public cherry-doom
  (package
   (name "cherry-doom")
   (version "ba823907")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/xemonix0/Cherry-Doom")
	   (commit "ba8239078fd41d5e5be8d4a160c26b0c0bd76da7")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "078gw4wcpnp8nlx9m20xcyw8b7xrq5rixvvp3ri7rhdqqw6qyhaw"))))
   (build-system cmake-build-system)
   (arguments
    (list #:tests? #f))
   (inputs (list fluidsynth
		 libsndfile
		 libxmp
		 openal
		 (sdl-union (list sdl2 sdl2-net))))
   (native-inputs (list python))
   (synopsis "Modern Boom/MBF Doom source port")
   (description "Cherry Doom is a fork of Nugget Doom intended to add even more features.")
   (home-page "https://github.com/xemonix0/Cherry-Doom")
   (properties '((tunable? . #t)))
   (license (list license:gpl2+         ; game, wad_stats, opl, textscreen
		  license:bsd-3         ; beta.h, u_scanner.*, FindSDL2
		  license:cc-by3.0      ; dogs.h
		  license:cc0           ; dogs.h
		  license:expat         ; nano_bsp, miniz
		  license:gpl3+         ; v_flextran, v_video
		  license:gpl2          ; soundfont
		  license:public-domain #; win_opendir ))))

(define-public eternity-engine
  (package
    (name "eternity-engine")
    (version "4.03.00pre")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/team-eternity/eternity")
	     (commit "9e3b6eb6fb229eae5debd13c3028552fd63b183d")
	     (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "0rcxmhhrrj84jc13p2hdy1bp4ncrw7j8ngzwsjy6j93qs36nph92"))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f))
    (inputs (list (sdl-union (list sdl2 sdl2-net sdl2-mixer))))
    (synopsis "Source port of DOOM")
    (description "Eternity is an advanced DOOM source port maintained by James \"Quasar\" Haley, descended from Simon \"fraggle\" Howard's SMMU.")
    (home-page "https://github.com/team-eternity/eternity")
    (properties '((tunable? . #t)))
    (license license:gpl3+)))

(define zmusic
  (package
   (name "zmusic")
   (version "1.1.13")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
                  (url "https://github.com/ZDoom/ZMusic/")
                  (commit version)))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
              "0d0r12a24s2glcwdl1ia6f48j98k03w4j8cfl4pp324wj95wryxf"))))
   (build-system cmake-build-system)
   (arguments
    '(#:tests? #f))
   (inputs (list alsa-lib glib))
   (native-inputs (list pkg-config))
   (synopsis "gzdoom music library")
   (description "Standalone music library from gzdoom")
   (home-page "https://github.com/ZDoom/ZMusic/")
   (properties '((tunable? . #t)))
   (license license:gpl3+)))

(define-public gzdoom
  (package
    (name "gzdoom")
    (version "4.11.3")
    (source (origin
	    (method git-fetch)
	    (uri (git-reference
                  (url "https://github.com/ZDoom/gzdoom")
                  (commit (string-append "g" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
              "13zg0rnm664mnczy8vsbcqiz9qx6f561c0qsi1395xmzfm3vk3x5"))))
    (arguments
     (list
      #:tests? #f
      #:build-type "Release"
      #:configure-flags #~(list (string-append "-DSHARE_DIR=" #$output "/share/")
				;; Link libraries at build time instead of loading them at run time.
				"-DDYN_OPENAL=OFF"
				"-DDYN_GTK=OFF")
      #:phases #~(modify-phases %standard-phases
		   (add-before 'configure 'fix-referenced-paths
		     (lambda _
		       (substitute* "src/CMakeLists.txt"
			 (("COMMAND /bin/sh")
			  (string-append "COMMAND " (which "sh")))))))))
    (build-system cmake-build-system)
    (inputs (list bzip2
		  fluid-3
		  fluidsynth
		  gtk+
		  libgme
		  libjpeg-turbo
		  libsndfile
		  libvpx
		  libwebp
		  mesa
		  mpg123
		  openal
		  sdl2
		  timidity++
		  zmusic
		  zlib))
    (native-inputs (list pkg-config unzip))
    (synopsis "Modern Doom 2 source port")
    (description
     "GZdoom is a port of the Doom 2 game engine, with a modern
renderer.  It improves modding support with ZDoom's advanced mapping features
and the new ZScript language.  In addition to Doom, it supports Heretic, Hexen,
Strife, Chex Quest, and fan-created games like Harmony, Hacx and Freedoom.")
    (home-page "https://zdoom.org/index")
    ;; The source uses x86 assembly
    (supported-systems '("x86_64-linux" "i686-linux"))
    (properties '((tunable? . #t)))
    (license (list license:gpl3+ ;gzdoom game
                   license:lgpl3+ ;gzdoom renderer
                   license:expat ;gdtoa
                   (license:non-copyleft ;modified dumb
                    "file://dumb/licence.txt"
                    "Dumb license, explicitly GPL compatible.")))))

(define-public doomrunner
  (package
   (name "doomrunner")
   (version "1.8.2")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
                  (url "https://github.com/Youda008/DoomRunner")
                  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "1szvhmv6giwv9npspk1fbr3s44j3566667q14gx2z7mqcnl3nir1"))))
   (build-system gnu-build-system)
   (arguments
    (list
     #:tests? #f
     #:phases
     #~(modify-phases %standard-phases
		      (replace 'configure
			       (lambda* (#:key outputs #:allow-other-keys)
				 (substitute* "DoomRunner.pro"
					      (("INSTALL_DIR = .*")
					       (string-append "INSTALL_DIR = " (assoc-ref %outputs "out") "/bin")))
				 (invoke "qmake" "-makefile" "-spec" "linux-g++" "CONFIG+=Release" "DoomRunner.pro"))))))
   (inputs (list qtbase-5))
   (synopsis "Launcher for managing Doom source ports")
   (description "Doom Runner is yet another launcher of common Doom source ports with graphical user interface. It is written in C++ and Qt, and it is designed around the idea of presets for various multi-file modifications to allow one-click switching between them and minimize any repetitive work.")
   (home-page "https://github.com/Youda008/DoomRunner")
   (license license:gpl3)))

(define-public doom64ex-plus
  (package
    (name "doom64ex-plus")
    (version "3.6.5.9")
    (source (origin
	      (method git-fetch)
	      (uri (git-reference
                    (url "https://github.com/atsb/Doom64EX-Plus/")
                    (commit version)))
	      (file-name (git-file-name name version))
	      (sha256
	       (base32
		"0hghiz60ncr08y6mglycd9045r1b6ds7x7h355dxf74byhhjpkns"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:tests? #f
      #:phases #~(modify-phases %standard-phases
		   (delete 'configure)
		   (replace 'build
		     (lambda* (#:key inputs outputs #:allow-other-keys)
		       (setenv "CFLAGS" "-DDOOM_UNIX_INSTALL")
		       (invoke "make")))
		   (replace 'install
		     (lambda _
		       (install-file "DOOM64EX-Plus"
				     (string-append #$output "/bin")))))))
    (inputs (list fluidsynth
		  glu
		  libpng
		  (sdl-union (list sdl2 sdl2-net))
		  zlib))
    (native-inputs (list pkg-config))
    (synopsis "Doom64 source port")
    (description "Doom 64 EX+ is a continuation project of Samuel \"Kaiser\" Villarreal's Doom 64 EX aimed to recreate DOOM 64 as closely as possible with additional modding features.")
    (home-page "https://github.com/atsb/Doom64EX-Plus/")
    (properties '((tunable? . #t)))
    (license license:gpl2+)))
