(define-module (laksek packages dns)
  #:use-module (guix packages)
  #:use-module (gnu packages python)
  #:use-module (gnu packages golang)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix build-system go))

(define-public dnscrypt-proxy2
  (package
    (name "dnscrypt-proxy2")
    (version "2.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/DNSCrypt/dnscrypt-proxy")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1k8d1qw5r0czp6daf66xg10zz27hkajv39yfks2c0kwrk2hzfrfj"))))
    (build-system go-build-system)
    (propagated-inputs `(("python" ,python)))
    (arguments
     `(#:import-path "github.com/dnscrypt/dnscrypt-proxy/dnscrypt-proxy"
       #:unpack-path "github.com/dnscrypt/dnscrypt-proxy"))
    (home-page "https://www.dnscrypt.org")
    (synopsis "Securely send DNS requests to a remote server")
    (description
     "A tool for securing communications
between a client and a DNS resolver.  It verifies that responses you get
from a DNS provider was actually sent by that provider, and haven't been
tampered with.  For optimal performance it is recommended to use this as
a forwarder for a caching DNS resolver such as dnsmasq, but it
can also be used as a normal DNS \"server\".")
    (license license:isc)))
