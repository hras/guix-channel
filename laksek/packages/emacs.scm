(define-module (laksek packages emacs)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-bqn-mode
  (package
    (name "emacs-bqn-mode")
    (version "2023.07.18")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/museoa/bqn-mode")
             (commit "cd7a9956a03bafbc4beff96246cad94779b953d1")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0glf8vmgchc3sqlk2v2aiyyhap3ym7nh1hdaz9hjvznjbwai12b0"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/museoa/bqn-mode")
    (synopsis "BQN mode for emacs")
    (description "BQN mode for emacs based on APL mode")
    (license license:gpl3+)))

(define-public emacs-merlin
  (package
    (name "emacs-merlin")
    (version "20230920.824")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://melpa.org/packages/merlin-" version ".tar"))
       (sha256
        (base32 "1fhj51mwma3armar68z402fygkhgwk9v3101d3rgz3sij922xsqx"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/ocaml/merlin")
    (synopsis "Mode for Merlin, an assistant for OCaml")
    (description
     "Merlin is an editor service that provides modern IDE features for OCaml.")
    (license license:expat)))

(define-public emacs-merlin-company
  (package
    (name "emacs-merlin-company")
    (version "20221123.1408")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://melpa.org/packages/merlin-company-"
                           version ".tar"))
       (sha256
        (base32 "1lsnk218v84p4r4zpj812zsc33r14l34qk3hwcjzli1jlvzmhagn"))))
    (build-system emacs-build-system)
    (inputs (list emacs-company emacs-merlin))
    (home-page "https://github.com/ocaml/merlin")
    (synopsis "Merlin and company mode integration")
    (description
     "Merlin is an editor service that provides modern IDE features for OCaml.")
    (license license:expat)))

(define-public emacs-decide-mode
  (package
    (name "emacs-decide-mode")
    (version "0.9.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/lifelike/decide-mode")
             (commit "cc67cd24791accd17a2656512d863e24ca3fd578")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0i28pgqw3rv4ak1rrf8zv5cbqil7gmdaycyir85lmry4axhcbmsc"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/lifelike/decide-mode")
    (synopsis "Random decisions for Emacs, with functions and a minor mode")
    (description
     "Roll dice and generate random numbers and text in a few other ways.
Mostly inspired by tools and activities related to being
a game master in role-playing games or playing solitaire games,
but may be useful beyond that.")
    (license license:gpl3+)))
